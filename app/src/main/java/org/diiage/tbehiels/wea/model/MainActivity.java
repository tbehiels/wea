package org.diiage.tbehiels.wea.model;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.diiage.tbehiels.wea.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
