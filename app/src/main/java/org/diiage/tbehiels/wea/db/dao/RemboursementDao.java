package org.diiage.tbehiels.wea.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import org.diiage.tbehiels.wea.db.entity.RemboursementEntity;

import java.util.List;

@Dao
public interface RemboursementDao {
    @Insert
    void insert(RemboursementEntity remboursementEntity);

    @Query("SELECT * FROM RemboursementEntity")
    LiveData<List<RemboursementEntity>> getAllRemboursement();

    @Query("DELETE FROM RemboursementEntity")
    void deleteAllRemboursement();
}
