package org.diiage.tbehiels.wea.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

public class StatutEntity {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="Id")
    public int uid;

    @NonNull
    @ColumnInfo(name="Label")
    public int label;

    public int getUid() {
        return uid;
    }

    public int getLabel() {
        return label;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public void setLabel(int label) {
        this.label = label;
    }
}
