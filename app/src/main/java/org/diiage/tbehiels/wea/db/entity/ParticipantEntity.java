package org.diiage.tbehiels.wea.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "participant")
public class ParticipantEntity {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @NonNull
    @ColumnInfo(name="Nom")
    public String nom;

    @ColumnInfo(name="Prenom")
    public String prenom;

    @ColumnInfo(name="Surnom")
    public String surnom;

    @ColumnInfo(name="Montant")
    public double montant;

    public int getUid(){
        return uid;
    }

    public String getNom(){
        return nom;
    }

    public String getPrenom(){
        return prenom;
    }

    public String getSurnom(){
        return surnom;
    }

    public void setUid(int uid){
        this.uid = uid;
    }

    public void setNom(String nom){
        this.nom = nom;
    }

    public void setPrenom(String prenom){
        this.prenom = prenom;
    }

    public void setSurnom(String surnom){
        this.surnom = surnom;
    }

    public void setMontant(double montant){
        this.montant = montant;
    }
}