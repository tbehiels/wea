package org.diiage.tbehiels.wea.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import java.util.Date;

public class SejourEntity {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @NonNull
    @ColumnInfo(name="Label")
    public String label;

    @NonNull
    @ColumnInfo(name="DateDebut")
    public Date dateDebut;

    @NonNull
    @ColumnInfo(name="DateFin")
    public Date dateFin;

    @NonNull
    @ColumnInfo(name="StatutId")
    public int statutId;

    public int getUid() {
        return uid;
    }

    public String getLabel() {
        return label;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public int getStatutId() {
        return statutId;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public void setStatutId(int statutId) {
        this.statutId = statutId;
    }
}
