package org.diiage.tbehiels.wea.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

public class Participant_SejourEntity {
    @NonNull
    @ColumnInfo(name="ParticipantId")
    public int participantId;

    @NonNull
    @ColumnInfo(name="SejourId")
    public int sejoutId;

    public int getParticipantId(){
        return participantId;
    }

    public int getSejoutId(){
        return sejoutId;
    }

    public void setParticipantId(int participantId){
        this.participantId = participantId;
    }

    public void setSejoutId(int sejoutId){
        this.sejoutId = sejoutId;
    }
}
