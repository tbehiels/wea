package org.diiage.tbehiels.wea.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

public class PosteDepenseEntity {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @NonNull
    @ColumnInfo(name="Label")
    public String label;

    @NonNull
    @ColumnInfo(name="Photo")
    public byte[] photo;

    @NonNull
    @ColumnInfo(name="Montant")
    public double montant;

    @ColumnInfo(name="ParticipantId")
    public int participantId;

    @NonNull
    @ColumnInfo(name="SejourId")
    public int sejourId;

    public int getUid(){
        return uid;
    }

    public String getLabel() {
        return label;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public double getMontant() {
        return montant;
    }

    public int getParticipantId() {
        return participantId;
    }

    public int getSejourId() {
        return sejourId;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public void setParticipantId(int participantId) {
        this.participantId = participantId;
    }

    public void setSejourId(int sejourId) {
        this.sejourId = sejourId;
    }
}
