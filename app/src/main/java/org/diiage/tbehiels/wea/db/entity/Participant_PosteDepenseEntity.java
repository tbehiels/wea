package org.diiage.tbehiels.wea.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

public class Participant_PosteDepenseEntity {
    @NonNull
    @ColumnInfo(name="ParticipantId")
    public int participantId;

    @NonNull
    @ColumnInfo(name="PosteDepenseId")
    public int posteDepenseId;

    @ColumnInfo(name="Montant")
    public double montant;

    public int getParticipantId(){
        return participantId;
    }

    public int getPosteDepenseId(){
        return posteDepenseId;
    }

    public double getMontant(){
        return montant;
    }

    public void setParticipantId(int participantId){
        this.participantId = participantId;
    }

    public void setPosteDepenseId(int posteDepenseId){
        this.posteDepenseId = posteDepenseId;
    }

    public void setMontant(double montant){
        this.montant = montant;
    }
}
