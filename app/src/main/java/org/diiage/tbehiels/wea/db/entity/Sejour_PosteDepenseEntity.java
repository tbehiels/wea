package org.diiage.tbehiels.wea.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

public class Sejour_PosteDepenseEntity {
    @NonNull
    @ColumnInfo(name="PosteDepenseId")
    public int posteDepenseId;

    @NonNull
    @ColumnInfo(name="SejourId")
    public int sejoutId;

    public int getPosteDepenseId() {
        return posteDepenseId;
    }

    public int getSejoutId() {
        return sejoutId;
    }

    public void setPosteDepenseId(int posteDepenseId) {
        this.posteDepenseId = posteDepenseId;
    }

    public void setSejoutId(int sejoutId) {
        this.sejoutId = sejoutId;
    }
}
