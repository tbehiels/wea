package org.diiage.tbehiels.wea.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

public class Participant_RemboursementEntity {
    @NonNull
    @ColumnInfo(name="ParticipantId")
    public int participantId;

    @NonNull
    @ColumnInfo(name="RemboursementId")
    public int remboursementId;

    public int getParticipantId(){
        return participantId;
    }

    public int getRemboursementId(){
        return remboursementId;
    }

    public void setParticipantId(int participantId){
        this.participantId = participantId;
    }

    public void setRemboursementId(int remboursementId){
        this.remboursementId = remboursementId;
    }
}
