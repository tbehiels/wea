package org.diiage.tbehiels.wea.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

public class Sejour_StatutEntity {
    @NonNull
    @ColumnInfo(name="SejourId")
    public int sejourId;

    @NonNull
    @ColumnInfo(name="StatutId")
    public int statutId;

    public int getSejourId() {
        return sejourId;
    }

    public int getStatutId() {
        return statutId;
    }

    public void setSejourId(int sejourId) {
        this.sejourId = sejourId;
    }

    public void setStatutId(int statutId) {
        this.statutId = statutId;
    }
}
