package org.diiage.tbehiels.wea.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "remboursement")
public class RemboursementEntity {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @NonNull
    @ColumnInfo(name = "Montant")
    public double montant;

    @NonNull
    @ColumnInfo(name = "EmetteurId")
    public int emetteurId;

    @NonNull
    @ColumnInfo(name = "ReceveurId")
    public int receveurId;

    public int getUid() {
        return uid;
    }

    public double getMontant() {
        return montant;
    }

    public int getEmetteurId() {
        return emetteurId;
    }

    public int getReceveurId() {
        return receveurId;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public void setEmetteurId(int emetteurId) {
        this.emetteurId = emetteurId;
    }

    public void setReceveurId(int receveurId) {
        this.receveurId = receveurId;
    }
}