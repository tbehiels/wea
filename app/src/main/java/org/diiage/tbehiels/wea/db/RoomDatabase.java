package org.diiage.tbehiels.wea.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.content.Context;

import org.diiage.tbehiels.wea.db.entity.ParticipantEntity;
import org.diiage.tbehiels.wea.db.entity.Participant_PosteDepenseEntity;
import org.diiage.tbehiels.wea.db.entity.Participant_RemboursementEntity;
import org.diiage.tbehiels.wea.db.entity.Participant_SejourEntity;
import org.diiage.tbehiels.wea.db.entity.PosteDepenseEntity;
import org.diiage.tbehiels.wea.db.entity.PosteDepense_RemboursementEntity;
import org.diiage.tbehiels.wea.db.entity.RemboursementEntity;
import org.diiage.tbehiels.wea.db.entity.SejourEntity;
import org.diiage.tbehiels.wea.db.entity.Sejour_PosteDepenseEntity;
import org.diiage.tbehiels.wea.db.entity.Sejour_StatutEntity;
import org.diiage.tbehiels.wea.db.entity.StatutEntity;
import org.diiage.tbehiels.wea.db.dao.RemboursementDao;

@Database(entities = {ParticipantEntity.class, Participant_PosteDepenseEntity.class, Participant_RemboursementEntity.class, Participant_SejourEntity.class, PosteDepenseEntity.class, PosteDepense_RemboursementEntity.class
, RemboursementEntity.class, SejourEntity.class, Sejour_PosteDepenseEntity.class, Sejour_StatutEntity.class, StatutEntity.class}, version = 1)
public abstract class RoomDatabase extends android.arch.persistence.room.RoomDatabase {
    public abstract RemboursementDao remboursementDao();

    private static volatile RoomDatabase INSTANCE;



    public static RoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (RoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RoomDatabase.class, "wea_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
