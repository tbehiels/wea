package org.diiage.tbehiels.wea.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

public class PosteDepense_RemboursementEntity {
    @NonNull
    @ColumnInfo(name="RemboursementId")
    public int remboursementId;

    @NonNull
    @ColumnInfo(name="PosteDepenseId")
    public int posteDepenseId;

    public int getRemboursementId() {
        return remboursementId;
    }

    public int getPosteDepenseId() {
        return posteDepenseId;
    }

    public void setRemboursementId(int remboursementId) {
        this.remboursementId = remboursementId;
    }

    public void setPosteDepenseId(int posteDepenseId) {
        this.posteDepenseId = posteDepenseId;
    }
}
