package org.diiage.tbehiels.wea.db.repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import org.diiage.tbehiels.wea.db.entity.RemboursementEntity;
import org.diiage.tbehiels.wea.db.dao.RemboursementDao;
import org.diiage.tbehiels.wea.db.RoomDatabase;

import java.util.List;

public class RemboursementRepository {
    private RemboursementDao remboursementDao;
    private LiveData<List<RemboursementEntity>> allRemboursements;

    RemboursementRepository(Application application){
        RoomDatabase db = RoomDatabase.getDatabase(application);
        remboursementDao = db.remboursementDao();
        allRemboursements = remboursementDao.getAllRemboursement();
    }

    LiveData<List<RemboursementEntity>> getAllRemboursements(){
        return allRemboursements;
    }

    /*public void insert (RemboursementEntity remboursementEntity){
        new insertAsyncTask(remboursementDao).execute(remboursementEntity);
    }

    private static class insertAsyncTask extends AsyncTask<RemboursementEntity, Void, Void> {

        private RemboursementDao mAsyncTaskDao;

        insertAsyncTask(RemboursementDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final RemboursementEntity... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }*/
}
