public abstract class RoomDatabase extends Object {
    protected List<RoomDatabase> roomDatabaseList;
    protected SupportSQLLiteDatabase supportSQLLiteDatabase;

    public RoomDatabase(){

    }

    public final boolean allowDestructiveMigrationOnDowngrade;

    public final List<RoomDatabase.Callback> callbacks;
    public final Context context;

    public final WeaDatabase;
}